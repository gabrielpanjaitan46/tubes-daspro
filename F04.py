import konversiCSVkeArray,tools

arrayGame = konversiCSVkeArray.CSVGameTOArray()

def tambahGame():
    global arrayGame
    game_nama = input("Masukkan nama game: ")
    game_kategori = input("Masukkan kategori: ")
    game_tahun_rilis = input("Masukkan tahun rilis: ")
    game_harga = input("Masukkan harga: ")
    game_stok = input("Masukkan stok awal: ")

    infoGameValid = True
    if game_nama == "" or game_kategori == "" or game_tahun_rilis == "" or game_harga == "" or game_stok == "":
        infoGameValid = False
    
    if infoGameValid == False :
        print("Mohon masukkan semua informasi mengenai game yang dapat disimpai di BNMO")
        tambahGame()
    else :
        id = tools.panjangArray(arrayGame)
        id = str(id)
        index = 0
        for i in id:
            index += 1
        
        arrayGame = arrayGame + [["GAME"+"0"*(3-index)+str(tools.panjangArray(arrayGame)), game_nama, game_kategori, game_tahun_rilis, game_harga, game_stok]]
        print(f'Selamat! Berhasil menambahkan game {game_nama}')

    return arrayGame

for i in range(3):
    print(tambahGame())
